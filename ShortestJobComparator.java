import java.util.Comparator;

class ShortestJobComparator implements Comparator<ProcessElement> {
    /* komparator do sortowania procesów względem czasu pozostałego
     * do wykonania */
    @Override
    public int compare(ProcessElement a, ProcessElement b) {
        double timeA = a.getTimeLeft();
        double timeB = b.getTimeLeft();

        if (timeA < timeB) {
            return -1;
        }
        else if (timeA > timeB) {
            return 1;
        }
        else {
            if (a.index < b.index) {
                return -1;
            } else if (a.index > b.index) {
                return 1;
            } else {
                assert (a == b);
                return 0;
            }
        }
    }
}
