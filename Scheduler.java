import java.util.ArrayList;

abstract class Scheduler {
    final Data input;
    ArrayList<ProcessElement> finishedProcesses;

    Scheduler(Data input) {
        this.input = new Data(input);
        finishedProcesses = new ArrayList<>();
    }

    public abstract void run();

    private double getAverageQueueTime() {
        assert(finishedProcesses != null);

        double sum = 0;
        double count = finishedProcesses.size();
        for (ProcessElement process : finishedProcesses) {
            sum += process.getQueueTime();
        }

        return sum / count;
    }

    private double getAverageWaitTime() {
        assert(finishedProcesses != null);

        int sum = 0;
        double count = finishedProcesses.size();
        for (ProcessElement process : finishedProcesses) {
            sum += process.getWaitingTime();
        }

        return sum / count;
    }

    public abstract void printSchedulerName();

    private void printAverageQueueTime() {
        System.out.printf("%.2f", getAverageQueueTime());
        System.out.print("\n");
    }

    private void printAverageWaitTime() {
        System.out.printf("%.2f", getAverageWaitTime());
        System.out.print("\n");
    }

    private void printProcessInfo() {
        for (ProcessElement process: finishedProcesses) {
            System.out.print("[" + process.index + " ");
            System.out.print(process.startTime + " ");
            System.out.printf("%.2f", process.getFinishTime());
            System.out.print("]");
        }
        System.out.print("\n");
    }

    void printResult() {
        if (finishedProcesses != null && finishedProcesses.size() > 0) {
            System.out.print("Strategia: ");
            printSchedulerName();
            printProcessInfo();
            System.out.print("Średni czas obrotu: ");
            printAverageQueueTime();
            System.out.print("Średni czas oczekiwania: ");
            printAverageWaitTime();
        }
    }
}
