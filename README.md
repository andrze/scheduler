# A scheduler simulation

Simulates the results of different process scheduling strategies
based on basic information about scheduled processes.

## implemented strategies:
- FCFS (First Come, First Served)
- SJF (Shortest Job First)
- SRT (Shortest Remaining Time)
- PS (Processor Sharing)
- RR (Round Robin)
 
## setup:
    javac Planista.java
    java Planista < sample_input.txt

## input (example):
    5 <- number of processes
    0 10 <- process #1: time of arrival at the queue, requested quantity of time
    0 29 <- etc.
    0 3
    0 7
    0 12 <- process #5: ...
    2 <- number of Round Robin schedulers
    1 10 <- Round Robin scheduler time quantities

## output (one block for each strategy):
    <strategy>: ...
    <description of processes>
    <average response time>
    <average waiting time>