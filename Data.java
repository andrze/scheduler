import java.util.ArrayList;
import java.util.Arrays;

class Data {
    private final int[][] processes;
    private int[] roundRobins;
    private ArrayList<ProcessElement> processesList;

    /* nowa struktura ArrayList z kopiami wartości poprzedniej */
    private static ArrayList<ProcessElement> deepCopy(ArrayList<ProcessElement> list) {
        ArrayList<ProcessElement> result = new ArrayList<>();
        for (ProcessElement process : list) {
            result.add(new ProcessElement(process));
        }
        return result;
    }

    private static int[][] copyProcesses(int[][] processes) {
        if (processes != null) {
            int[][] result = new int[processes.length][2];
            for (int i = 0; i < processes.length; i++) {
                assert (processes[i].length == 2);
                result[i] = Arrays.copyOf(processes[i], processes[i].length);
            }

            return result;
        }
        else {
            return null;
        }
    }

    Data(int[][] processes, int[] roundRobins) {
        this.processes = copyProcesses(processes);
        if (roundRobins != null) {
            this.roundRobins = Arrays.copyOf(roundRobins, roundRobins.length);
        }

        if (processes != null) {
            processesList = new ArrayList<>();
            for (int i = 0; i < processes.length; i++) {
                processesList.add(new ProcessElement(processes[i], i + 1));
            }
            processesList.sort(new InputSortComparator());
        }
    }

    Data(Data other) {
        this.processes = copyProcesses(other.processes);
        if (other.roundRobins != null) {
            this.roundRobins = Arrays.copyOf(other.roundRobins, other.roundRobins.length);
        }
        if (processes != null) {
            this.processesList = Data.deepCopy(other.processesList);
        }
    }

    int[] getRoundRobins() {
        if (roundRobins == null) {
            return null;
        }
        else {
            return Arrays.copyOf(roundRobins, roundRobins.length);
        }
    }

    ArrayList<ProcessElement> getAllProcesses() {
        if (processesList != null) {
            return processesList = Data.deepCopy(processesList);
        }
        else {
            return null;
        }
    }

    int numberOfProcesses() {
        if (processes != null) {
            return processes.length;
        }
        else {
            return 0;
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("Processes: ");

        for (ProcessElement process : processesList) {
            result.append(process.toString());
            result.append("\n");
        }

        result.append("Round robins: ");

        if (roundRobins != null) {
            for (int roundRobin : roundRobins) {
                result.append(roundRobin);
                result.append(" ");
            }
            result.append("\n");
        }

        return new String(result);
    }
}
