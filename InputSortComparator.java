import java.util.Comparator;

class InputSortComparator implements Comparator<ProcessElement> {
    /* komparator do sortowania danych wejściowych */
    @Override
    public int compare(ProcessElement a, ProcessElement b) {
        if (a.startTime < b.startTime) {
            return -1;
        }
        else if (a.startTime > b.startTime) {
            return 1;
        }
        else {
            if (a.index < b.index) {
                return -1;
            } else if (a.index > b.index) {
                return 1;
            } else {
                assert (a == b);
                return 0;
            }
        }
    }
}
