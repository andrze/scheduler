import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class SRT extends Scheduler {
    private final PriorityQueue<ProcessElement> queue;

    public SRT(Data input) {
        super(input);
        Comparator<ProcessElement> comparator = new ShortestJobComparator();
        queue = new PriorityQueue<>(10, comparator);
    }

    @Override
    public void run() {
        int processesCount = input.numberOfProcesses();
        int processArrayIndex = 0;
        ArrayList<ProcessElement> processes = input.getAllProcesses();
        ProcessElement running = null;
        int time = 0;

        while (processArrayIndex < processesCount || !queue.isEmpty()) {
            /* dodanie nowych procesów */
            for (; processArrayIndex < processesCount && processes.get(processArrayIndex).startTime == time;
                 processArrayIndex++) {
                ProcessElement newProcess = processes.get(processArrayIndex);
                if (running == null && queue.isEmpty()) {
                    running = newProcess;
                }
                else if (running == null && !queue.isEmpty()) {
                    assert queue.peek() != null;
                    if (newProcess.getTimeLeft() < queue.peek().getTimeLeft()) {
                        running = newProcess;
                    }
                    else {
                        queue.add(newProcess);
                        running = queue.poll();
                    }
                }
                else if (newProcess.getTimeLeft() < running.getTimeLeft()) {
                    queue.add(running);
                    running = newProcess;
                }
                else {
                    queue.add(newProcess);
                }
            }

            if (running == null) {
                /* proces o najmniejszym zapotrzebowaniu na czas */
                running = queue.poll();
            }

            assert running != null;

            double timeQuantity;

            if (processArrayIndex >= processes.size()) {
                /* nie ma już procesów oczekujących na wejście do kolejki */
                timeQuantity = running.getTimeLeft();
            }
            else {
                ProcessElement nextProcess = processes.get(processArrayIndex);
                timeQuantity = Math.min(running.getTimeLeft(), nextProcess.startTime - time);
            }

            /* przydzielenie czasu procesowi */
            running.giveProcessorTime(timeQuantity);
            time += timeQuantity;

            if (running.getTimeLeft() == 0) {
                /* zdjęcie zakończonego procesu z kolejki */
                running.setFinishTime(time);
                finishedProcesses.add(running);
                running = null;
            }
        }

        /* wyliczenie czasów wykonania */
        for (ProcessElement process : finishedProcesses) {
            process.evaluateResultTimes();
        }
    }

    @Override
    public void printSchedulerName() {
        System.out.println("SRT");
    }
}
