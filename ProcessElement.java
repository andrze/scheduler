class ProcessElement {
    /* klasa przechowująca dane o pojedyńczym procesie */

    final int index; // indeks procesu
    private final int time; // zapotrzebowanie na procesor
    final int startTime; // czas rozpoczęcia procesu
    private double timeLeft; // czas pozostały do skończenia procesu
    private double waitingTime; // czas oczekiwania
    private double queueTime; // czas obrotu
    private double finishTime; // czas wyjścia z kolejki

    ProcessElement(int index, int startTime, int time) {
        this.index = index;
        this.startTime = startTime;
        this.time = time;
        timeLeft = time;
        waitingTime = 0;
        queueTime = 0;
    }


    ProcessElement(int[] processes, int index) {
        this.index = index;
        this.startTime = processes[0];
        this.time = processes[1];
        timeLeft = processes[1];
        waitingTime = 0;
        queueTime = 0;
    }

    ProcessElement(ProcessElement other) {
        this.index = other.index;
        this.startTime = other.startTime;
        this.time = other.time;
        this.timeLeft = other.timeLeft;
        this.waitingTime = other.waitingTime;
        this.queueTime = other.queueTime;
        this.finishTime = other.finishTime;
    }

    double getTimeLeft() {
        return timeLeft;
    }

    boolean hasTimeLeft() {
        return timeLeft > 0;
    }

    /* przydzielenie czasu procesora */
    void giveProcessorTime(double x) {
        double processorTime = x;

        if (timeLeft < processorTime) {
            processorTime = timeLeft;
        }

        timeLeft -= processorTime;
    }

    void setFinishTime(double finishTime) {
        this.finishTime = finishTime;
    }

    void setWaitingTime(double x) {
        waitingTime = x;
    }

    private void evaluateQueueTime() {
        queueTime = finishTime - startTime;
    }

    private void evaluateWaitingTime() {
        waitingTime = finishTime - startTime - time;
    }

    /* wyliczenie czasu obrotu i oczekiwania */
    void evaluateResultTimes() {
        evaluateQueueTime();
        evaluateWaitingTime();
    }

    double getFinishTime() {
        return finishTime;
    }

    double getQueueTime() {
        return queueTime;
    }

    double getWaitingTime() {
        return waitingTime;
    }

    @Override
    public String toString() {
        return String.valueOf(index) + " " + String.valueOf(startTime) + " " + String.valueOf(time);
    }
}
