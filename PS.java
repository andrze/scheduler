import java.util.ArrayList;
import java.util.PriorityQueue;

public class PS extends Scheduler {
    private final PriorityQueue<ProcessElement> queue;

    /* równość liczb double z dokładnością do epsilona */
    private static boolean equals(double a, double b) {
        double epsilon = 0.000000001;
        return Math.abs(a - b) < epsilon;
    }

    /* nierówność liczb double z dokładnością do epsilona */
    private static boolean lessThanOrEqual(double a, double b) {
        return a < b || equals(a, b);
    }

    PS(Data input) {
        super(input);
        queue = new PriorityQueue<>(10, new ShortestJobComparator());
    }

    @Override
    public void run() {
        int processesCount = input.numberOfProcesses();
        int processArrayIndex = 0;
        ArrayList<ProcessElement> processes = input.getAllProcesses();
        ProcessElement shortest = null;
        double time = 0;
        int n;
        double nth; // n-ta część czasu procesora przydzielona pojedynczemu procesowi

        while (processArrayIndex < processesCount || !queue.isEmpty()) {
            /* dodanie nowych procesów */
            for (; processArrayIndex < processesCount && equals(processes.get(processArrayIndex).startTime, time);
                 processArrayIndex++) {
                queue.add(processes.get(processArrayIndex));
            }

            if (!queue.isEmpty()) {
                /* proces o najmniejszym zapotrzebowaniu na czas */
                shortest = queue.peek();
            }
            assert shortest != null;

            n = queue.size();
            nth = 1.0 / n;

            /* jednostka czasu przydzielona każdemu procesowi */
            double timeQuantity;

            if (processArrayIndex >= processes.size()) {
                /* nie ma już procesów oczekujących na wejście do kolejki */
                timeQuantity = shortest.getTimeLeft();
                time += timeQuantity / nth;
            }
            else {
                ProcessElement nextProcess = processes.get(processArrayIndex);
                double timeToNext = nextProcess.startTime - time;
                double shortestRunTime = shortest.getTimeLeft();

                if (lessThanOrEqual(timeToNext, shortestRunTime)) {
                    /* następnym wydarzeniem będzie dojście nowego procesu */
                    timeQuantity = timeToNext * nth;
                    time += timeToNext;
                }
                else {
                    /* następnym wydarzeniem będzie skończenie działania przez pewien proces */
                    timeQuantity = shortestRunTime * nth;
                    time += shortestRunTime;
                }
            }

            /* przydzielenie czasu procesom */
            for (ProcessElement process : queue) {
                process.giveProcessorTime(timeQuantity);
            }

            if (equals(shortest.getTimeLeft(), 0)) {
                /* usunięcie skończonego procesu z kolejki */
                shortest.setFinishTime(time);
                finishedProcesses.add(shortest);
                queue.poll();
                while (queue.peek() != null && equals(queue.peek().getTimeLeft(), 0)) {
                    queue.peek().setFinishTime(time);
                    finishedProcesses.add(queue.poll());
                }
            }
        }

        /* wyliczenie czasów wykonania */
        for (ProcessElement process : finishedProcesses) {
            process.evaluateResultTimes();
            process.setWaitingTime(0);
        }
    }

    @Override
    public void printSchedulerName() {
        System.out.println("PS");
    }
}
