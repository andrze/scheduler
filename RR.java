import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

public class RR extends Scheduler {
    private final int q; // czas przydzielony procesowi z początku kolejki
    private Deque<ProcessElement> queue;

    RR(Data input, int q) {
        super(input);
        this.q = q;
        queue = new LinkedList<>();
    }

    @Override
    public void run() {
        int processesCount = input.numberOfProcesses();
        int processArrayIndex = 0;
        ArrayList<ProcessElement> processes = input.getAllProcesses();
        ProcessElement running = null;
        int time = 0;
        double qLeft = 0; // część przydzielonej ilości czasu q, która nie została jeszcze wykorzystana

        while (processArrayIndex < processesCount || !queue.isEmpty()) {
            /* dodanie nowych procesów */
            for (; processArrayIndex < processesCount && processes.get(processArrayIndex).startTime <= time;
                 processArrayIndex++) {
                ProcessElement newProcess = processes.get(processArrayIndex);
                if (running == null && queue.isEmpty()) {
                    running = newProcess;
                    qLeft = q;
                }
                else {
                    queue.add(newProcess);
                }
            }

            if (running == null) {
                running = queue.poll();
                qLeft = q;
            }

            assert running != null;

            /* ilość czasu przydzielona procesowi z początku kolejki */
            double timeQuantity;

            if (processArrayIndex >= processes.size() && queue.isEmpty()) {
                /* nie ma już procesów oczekujących na wejście do kolejki */
                timeQuantity = running.getTimeLeft();
            }
            else if (processArrayIndex >= processes.size() && !queue.isEmpty()) {
                /* nie ma już procesów oczekujących na wejście do kolejki */
                timeQuantity  = Math.min(running.getTimeLeft(), qLeft);
            }
            else {
                ProcessElement nextProcess = processes.get(processArrayIndex);
                double timeToNextProcess = nextProcess.startTime - time;
                double minTime = Math.min(timeToNextProcess, running.getTimeLeft());
                timeQuantity = Math.min(minTime, qLeft);
            }

            /* przydzielenie czasu procesowi */
            running.giveProcessorTime(timeQuantity);
            qLeft -= timeQuantity;
            time += timeQuantity;

            if (running.getTimeLeft() == 0) {
                /* koniec wykonania procesu */
                running.setFinishTime(time);
                finishedProcesses.add(running);
                running = null;
            }
            else if (qLeft == 0) {
                /* koniec czasu przeznaczonego na dany proces */
                queue.add(running);
                running = null;
            }
        }

        /* wyliczenie czasów wykonania */
        for (ProcessElement process : finishedProcesses) {
            process.evaluateResultTimes();
        }
    }

    @Override
    public void printSchedulerName() {
        System.out.println("RR-" + q);
    }
}
