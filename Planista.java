import java.util.ArrayList;

public class Planista {
    private static ArrayList<Scheduler> getAllSchedulers(Data data) {
        /* tworzenie i dodawanie strategii */
        ArrayList<Scheduler> schedulers = new ArrayList<>();
        schedulers.add(new FCFS(data));
        schedulers.add(new SJF(data));
        schedulers.add(new SRT(data));
        schedulers.add(new PS(data));

        int[] roundRobins = data.getRoundRobins();

        if (roundRobins != null && roundRobins.length > 0) {
            for (int roundRobinQ : roundRobins) {
                schedulers.add(new RR(data, roundRobinQ));
            }
        }

        return schedulers;
    }

    private static void runSchedulers(ArrayList<Scheduler> schedulers) {
        /* symulowanie działania procesora */
        for (int i = 0; i < schedulers.size(); i++) {
            Scheduler scheduler = schedulers.get(i);
            scheduler.run();
            scheduler.printResult();
            System.out.print("\n");
        }
    }


    public static void main(String[] args) {
        Read input = null;

        if (args.length == 0) {
            input = new Read();
        }
        else if (args.length == 1) {
            input = new Read(args[0]);
        }
        else {
            System.out.print("Niepoprawna liczba parametrów: " + args.length + " (zamiast 0 lub 1)");
        }

        if (input != null) {
            Data data = input.read();
            input.close();


            ArrayList<Scheduler> schedulers = getAllSchedulers(data);
            runSchedulers(schedulers);

        }
        else {
            System.out.println("Błąd wczytywania danych.");
            System.exit(1);
        }
    }
}
