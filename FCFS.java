import java.util.ArrayList;

public class FCFS extends Scheduler {
    private final ArrayList<ProcessElement> queue;

    FCFS(Data input) {
        super(input);
        queue = new ArrayList<>();
    }

    @Override
    public void run() {
        int processesCount = input.numberOfProcesses();
        int processArrayIndex = 0; // indeks procesu, który jako następny pojawi się w kolejce
        ArrayList<ProcessElement> processes = input.getAllProcesses();
        ProcessElement running = null; // proces, który aktualnie zużywa zasoby procesora
        int time = 0; // zegar procesora

        while (processArrayIndex < processesCount || !queue.isEmpty()) {
            /* dodanie nowych procesów, które się pojawiają przy danej wartości time */
            for (; processArrayIndex < processesCount && processes.get(processArrayIndex).startTime == time;
                 processArrayIndex++) {
                queue.add(processes.get(processArrayIndex));
            }

            /* aktualizacja procesu używającego procesor */
            if (running == null && !queue.isEmpty()) {
                /* get first process */
                running = queue.get(0);
                queue.remove(0);
            }
            assert running != null;

            double timeQuantity; // czas przydzielony procesowi

            if (processArrayIndex >= processes.size()) {
                /* do kolejki nie dojdą już żadne now procesy */
                timeQuantity = running.getTimeLeft();
            }
            else {
                /* są jeszcze procesy, które dopiero dojdą do kolejki */
                ProcessElement nextProcess = processes.get(processArrayIndex);
                timeQuantity = Math.min(running.getTimeLeft(), nextProcess.startTime - time);
            }

            running.giveProcessorTime(timeQuantity); // zużycie zasobów procesora
            time += timeQuantity; // aktualizacja zegara procesora

            if (running.getTimeLeft() == 0) {
                /* zdjęcie zakończonego procesu z kolejki */
                running.setFinishTime(time);
                finishedProcesses.add(running);
                running = null;
            }
        }

        /* wyliczenie czasów czekania w kolejce */
        for (ProcessElement process : finishedProcesses) {
            process.evaluateResultTimes();
        }
    }

    @Override
    public void printSchedulerName() {
        System.out.println("FCFS");
    }
}
