import java.io.File;
import java.util.Scanner;

class Read {
    private Scanner input;
    private int lineNumber; // numer wiersza
    private static final String NOT_ENOUGH_DATA = "za mało danych";

    private static void printErrorMessage(int lineNumber, String message) {
        System.out.println("Błąd w wierszu " + lineNumber + ": " + message + ".");
    }

    Read(String path) {
        /* wczytywanie z pliku */
        lineNumber = 1;

        try {
            this.input = new Scanner(new File(path));
        } catch (Exception e) {
            System.out.println("Plik z danymi nie jest dostępny.");
            exit();
        }
    }

    Read() {
        /* wczytywanie ze standardowego wejścia */
        lineNumber = 1;

        try {
            input = new Scanner(System.in);
        } catch (Exception e) {
            System.out.println("Błąd wczytywania danych.");
            exit();
        }
    }

    private static void exitWithMessage(int lineNumber, String message) {
        printErrorMessage(lineNumber, message);
        System.exit(1);
    }

    private static void exit() {
        System.exit(1);
    }

    Data read() {
        int processesCount = getNumber();
        int[][] processes = getProcesses(processesCount);
        int robinsCount = getNumber();
        int[] roundRobins = getRoundRobins(robinsCount);

        if (input.hasNext()) {
            exitWithMessage(lineNumber, "za dużo danych");
        }
        return new Data(processes, roundRobins);
    }

    /* wczytuje wiersz z jedną liczbą */
    private int getNumber() {
        int result = 0;

        if (input.hasNextLine()) {
            String line = input.nextLine();

            if (validNumber(line)) {
                result = Integer.parseInt(line);
            }
        }
        else {
            exitWithMessage(lineNumber, NOT_ENOUGH_DATA);
        }
        lineNumber++;
        return result;
    }

    /* wczytuje procesy */
    private int[][] getProcesses(int processesCount) {
        if (processesCount == 0) {
            return null;
        }
        else if (processesCount > 0) {
            int[][] processes = new int[processesCount][2];

            for (int i = 0; i < processesCount; i++) {
                if (input.hasNextLine()) {
                    String line = input.nextLine();

                    if (validProcess(line)) {
                        String[] numbers = line.split(" ");
                        assert (numbers.length == 2);

                        for (int j = 0; j < numbers.length; j++) {
                            processes[i][j] = Integer.parseInt(numbers[j]);
                        }
                    }

                    lineNumber++;
                } else {
                    exitWithMessage(lineNumber, NOT_ENOUGH_DATA);
                }
            }
            return processes;
        }
        else {
            exitWithMessage(lineNumber, "ujemna liczba parametrów");
            return null;
        }
    }

    /* wczytuje strategie round robin */
    private int[] getRoundRobins(int robinsCount) {
        if (robinsCount == 0) {
            return null;
        }
        else if (robinsCount < 0) {
            exitWithMessage(lineNumber, "liczba ujemna");
            return null;
        }
        else if (robinsCount > 0) {
            int[] robins = new int[robinsCount];

            if (input.hasNextLine()) {
                String line = input.nextLine();

                if (validRoundRobin(line)) {
                    String[] numbers = line.split(" ");

                    if (numbers.length > robinsCount) {
                        exitWithMessage(lineNumber, "za dużo danych");
                    } else if (numbers.length < robinsCount) {
                        exitWithMessage(lineNumber, "za mało danych");
                    } else {
                        for (int i = 0; i < numbers.length; i++) {
                            robins[i] = Integer.parseInt(numbers[i]);
                            if (robins[i] <= 0) {
                                exitWithMessage(lineNumber, "liczba niedodatnia");
                            }
                        }
                    }
                }

                lineNumber++;
            } else {
                exitWithMessage(lineNumber, NOT_ENOUGH_DATA);
            }

            return robins;
        }
        else {
            exitWithMessage(lineNumber, "ujemna liczba parametrów");
            return null;
        }
    }

    /* czy napis reprezentuje liczbę */
    private boolean validNumber(String s) {
        if (s.matches("\\d+")) {
            return true;
        }
        else {
            if (s.matches("\\d+.+")) {
                exitWithMessage(lineNumber, "za dużo danych");
            } else if (s.matches(".*[^\\d].*")) {
                /* pojawił się znak, który nie jest spacją ani liczbą */
                exitWithMessage(lineNumber, "niepoprawne znaki na wejściu");
            }
            else {
              exitWithMessage(lineNumber, "błędne dane");
            }
            return false;
        }
    }

    private boolean validProcess(String s) {
        if (s.matches("\\d+ \\d+")) {
            return true;
        }
        else {
            if (s.matches("\\d+")) {
                /* jedna liczba */
                exitWithMessage(lineNumber, "za mało danych");
            } else if (s.matches("\\d+ \\d+.+")) {
                exitWithMessage(lineNumber, "za dużo danych");
            } else if (s.matches(".*[^\\d ].*")) {
                /* pojawił się znak, który nie jest spacją ani liczbą */
                exitWithMessage(lineNumber, "niepoprawne znaki na wejściu");
            } else {
                exitWithMessage(lineNumber, "błędne dane");
            }
            return false;
        }
    }

    private boolean validRoundRobin(String s) {
        if (s.matches("(\\d+ )*\\d+")) {
            return true;
        }
        else {
            if (s.matches(".*[^\\d ].*")) {
                /* pojawił się znak, który nie jest spacją ani liczbą */
                exitWithMessage(lineNumber, "niepoprawne znaki na wejściu");
            }
            else {
                exitWithMessage(lineNumber, "błędne dane");
            }

            return false;
        }
    }

    void close() {
        input.close();
    }
}
